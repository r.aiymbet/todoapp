//
//  DataStore.swift
//  ToDoApp
//
//  Created by Raiymbek Aiymbet on 1/20/21.
//

import Foundation
import SwiftUI
import Combine

struct Task: Identifiable {
    var id = String()
    var toDoItem = String()
    
    //fixes upcoming
}

class TaskStore: ObservableObject {
    @Published var tasks = [Task]()
}

